---------------------------------------------------------------------------------------------
-- author: 	Christian Blosser							   --
-- file:	PortfolioProjectThree.sql						   --
-- date created: 10-01-2020								   --
-- MySQL version:	8.0.21-0 ubuntu 0.20.04.4					   --
--											   --
-- description:										   --
--	This SQL file is for use of my personal ePortfolio to showcase understanding of    --
--	advanced MySQL concepts/statements.  When executed, the code contained in this	   --
--	file will display a set of pre-made queries specific to the skills being showcased --
--	Specifically, this program will create a database named university, use the new	   --
--	database, create a students table, implement triggers, create views, index the	   --
--	database, utilize full-text querying, and populate the database with sample rows   --
--											   --
--	DATABASE NAME:		university						   --
--	university TABLE:	students						   --
--	students colums:	student_id, first_name, last_name, gender,		   --
--				age, birthday, salary, favorite_quote			   --
---------------------------------------------------------------------------------------------

-- CREATE database if it does not already exist
CREATE DATABASE IF NOT EXISTS university;

-- USE created database
USE university;

-- CREATE students table with all column characteristics
CREATE TABLE students(
	student_id INT(6) NOT NULL auto_increment,
	first_name VARCHAR(25) NOT NULL default 'first name needed',
	last_name VARCHAR(25) NOT NULL default 'last name needed',
	gender VARCHAR(1) default NULL,
	age INT(8) NOT NULL default 0,
	birthday DATE default NULL,
	salary INT(8) NOT NULL default 0,
	favorite_quote VARCHAR(250) default NULL,
	PRIMARY KEY (student_id)
	) AUTO_INCREMENT=1;

-- change delimiter type in order to create IF conditional statement
DELIMITER //

-- CREATE TRIGGER to verify the age column of new entries.  If the age is negative
-- this trigger will return the absolute value.  This is assuming a typo with user
-- input and is corrected.  Age can never be negative
CREATE TRIGGER before_verify_age
	BEFORE INSERT ON students
	FOR EACH ROW IF NEW.age < 0
	THEN SET NEW.age = ABS(NEW.age);
	END IF; //

-- CREATE TRIGGER to verify the salary column of new entries.  If salaray is
-- negative, this trigger will return a zero in place of what is entered.
CREATE TRIGGER before_verify_salary
	BEFORE INSERT ON students
	FOR EACH ROW IF NEW.salary < 0
	THEN SET NEW.salary = 0;
	END IF; //

-- revert delimiter to previous default delimiter of semi-colon
DELIMITER ;

-- CREATE VIEW based on daily earnings for a person.  This view displays a students
-- first name, last name, and their daily earnings based on their salary
CREATE VIEW daily_earnings
	AS
	SELECT
    		first_name, last_name, CONCAT('$', salary/365)
	FROM
    		students
	WHERE
		salary > 0;

-- CREATE VIEW to display normal student information
CREATE VIEW student_info
	AS
	SELECT
		student_id, last_name, first_name, age, gender
	FROM
		students;



-- CREATE INDEX on last name
CREATE INDEX last_name_index ON students (last_name);


-- CREATE INDEX on salary and student id
CREATE INDEX working_students ON students (salary, student_id);


-- insert entries into students table.  The first will verify the creation of
-- before_verify_age trigger, the second will verify the creation of before_
-- verify_salary trigger.
INSERT INTO students (first_name, last_name, gender, age, birthday, salary, favorite_quote)
	VALUES  ('Ringo', 'Starr', 'm', -20, 20000401, 26300, 'Of course Im ambitious. Whats wrong with that? Otherwise you sleep all day.'),
        	('Eddy', 'Van Halen', 'm', 18, 20021001, -12555, 'Music kept me off the streets and out of trouble and gave me something that was mine that no one could take away from me.'),
        	('Julia', 'Roberts', 'f', 19, 20021221, 0, 'Im just a girl, standing in front of a boy, asking him to love her.');

-- query all rows from table to verify proper trigger implementation using last name index
-- displays 3 correct results
SELECT * FROM students USE INDEX(last_name_index);

-- query all rows from table using salary and student id index
SELECT * FROM students USE INDEX(working_students);


-- query created view to display all daily earnings
-- displays 2 results
SELECT * FROM daily_earnings;


-- query created view to display all students using index
-- displays 3 results
SELECT * FROM student_info;


-- query to show the triggers created for the university database
-- displays 2 different triggers
SHOW TRIGGERS FROM university;

-- query to drop a trigger
DROP TRIGGER before_verify_salary;

-- query to show the triggers created for the university database
-- displays 1 trigger
SHOW TRIGGERS FROM university;

-- query to show all views implemented on table students
-- displays 2 views
SHOW FULL TABLES WHERE table_type = 'VIEW';

-- query to drop a view
DROP VIEW daily_earnings;
-- query to show all views implemented on table students
-- displays 1 view
SHOW FULL TABLES WHERE table_type = 'VIEW';

-- query to show all indexes implemented on table students
-- displays 2 indexes
SHOW INDEXES FROM university.students;

-- qyery to drop an index
DROP INDEX working_students ON university.students;

-- query to show all indexes implemented on table students
-- displays 2 indexes
SHOW INDEXES FROM university.students;

-- ALTER table to implement full-text searching.
ALTER TABLE students
    ADD FULLTEXT(favorite_quote);
    
-- query full text searching 
SELECT
    last_name, first_name
FROM
    students
WHERE
    MATCH (favorite_quote)
    AGAINST ('love')
ORDER BY
    student_id;






